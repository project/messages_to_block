Description
---------------
Message to Block allows you to supress drupal_set_message()'s,
at which point they are saved to be displayed and/or dismissed from a block.

Configuration
---------------
All settings are under:
Configuration -> User Interface -> Messages to Block settings

Settings for which messages, and which users to affect:
*Ignore user uid 1.
*Ignore users of chosen certain role(s).
*By message type (status, warning, error).

Configure output:
*Table view
*List view

Author Info:
---------------
Joshua Walker (drastik) of http://drastikbydesign.com
